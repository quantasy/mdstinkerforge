/*
 * /*
 *  *   "TiMqWay"
 *  *
 *  *    TiMqWay(tm): A gateway to provide an MQTT-View for the Tinkerforge(tm) world (Tinkerforge-MQTT-Gateway).
 *  *
 *  *    Copyright (c) 2016 Bern University of Applied Sciences (BFH),
 *  *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *    Quellgasse 21, CH-2501 Biel, Switzerland
 *  *
 *  *    Licensed under Dual License consisting of:
 *  *    1. GNU Affero General Public License (AGPL) v3
 *  *    and
 *  *    2. Commercial license
 *  *
 *  *
 *  *    1. This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU Affero General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU Affero General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU Affero General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  *
 *  *
 *  *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *  *     accordance with the commercial license agreement provided with the
 *  *     Software or, alternatively, in accordance with the terms contained in
 *  *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *  *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *  *
 *  *
 *  *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *  *
 *  *
 */
package ch.quantasy.gateway.service.tinkerforge;

import ch.quantasy.gateway.binding.tinkerforge.accelerometerV2.AccelerationCallbackConfiguration;
import ch.quantasy.gateway.binding.tinkerforge.accelerometerV2.AccelerationCallbackConfigurationStatus;
import ch.quantasy.gateway.binding.tinkerforge.accelerometerV2.AccelerometerEvent;
import ch.quantasy.gateway.binding.tinkerforge.accelerometerV2.AccelerometerV2ServiceContract;
import ch.quantasy.gateway.binding.tinkerforge.accelerometerV2.ContinuousAccelerationCallbackConfigurationStatus;
import ch.quantasy.gateway.binding.tinkerforge.accelerometerV2.ContinuousAccelerationConfiguration;
import ch.quantasy.gateway.binding.tinkerforge.accelerometerV2.ContinuousAccelerationEvent;
import ch.quantasy.gateway.binding.tinkerforge.accelerometerV2.DeviceConfigurationStatus;
import ch.quantasy.tinkerforge.device.accelerometerV2.AccelerometerV2Device;
import ch.quantasy.tinkerforge.device.accelerometerV2.AccelerometerV2DeviceCallback;
import java.net.URI;

/**
 *
 * @author reto
 */
public class AccelerometerV2Service extends AbstractDeviceService<AccelerometerV2Device, AccelerometerV2ServiceContract> implements AccelerometerV2DeviceCallback {

    public AccelerometerV2Service(AccelerometerV2Device device, URI mqttURI)  {
        super(mqttURI, device, new AccelerometerV2ServiceContract(device));
    }

    @Override
    public void continuousAccelerationConfigChanged(ContinuousAccelerationConfiguration continuousAccelerationConfig) {
                readyToPublishStatus(getContract().STATUS_CONTINUOUS_ACCELERATION_CONFIGURATION, new ContinuousAccelerationCallbackConfigurationStatus(continuousAccelerationConfig));

    }

    @Override
    public void accelerationCallbackConfigurationChanged(AccelerationCallbackConfiguration accelerationCallbackConfiguration) {
                readyToPublishStatus(getContract().STATUS_ACCELERATION_CALLBACK_CONFIGURATION, new AccelerationCallbackConfigurationStatus(accelerationCallbackConfiguration));
    }

    @Override
    public void deviceConfigChanged(ch.quantasy.gateway.binding.tinkerforge.accelerometerV2.DeviceConfiguration deviceConfig) {
                readyToPublishStatus(getContract().STATUS_DEVICE_CONFIGURATION, new DeviceConfigurationStatus(deviceConfig));
    }

    @Override
    public void acceleration(int arg0, int arg1, int arg2) {
                readyToPublishEvent(getContract().EVENT_ACCELERATION, new AccelerometerEvent(arg0,arg1,arg2));
    }

    @Override
    public void continuousAcceleration16Bit(int[] arg0) {
                readyToPublishEvent(getContract().EVENT_CONTINUOUS_ACCELERATION_BIT16, new ContinuousAccelerationEvent(arg0));
    }

    @Override
    public void continuousAcceleration8Bit(int[] arg0) {
                readyToPublishEvent(getContract().EVENT_CONTINUOUS_ACCELERATION_BIT16, new ContinuousAccelerationEvent(arg0));
    }

    
    
}
