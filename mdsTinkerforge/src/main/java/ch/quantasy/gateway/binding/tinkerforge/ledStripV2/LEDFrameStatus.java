package ch.quantasy.gateway.binding.tinkerforge.ledStripV2;

import ch.quantasy.mdservice.message.AStatus;

public class LEDFrameStatus extends AStatus {

    public LEDFrame value;

    private LEDFrameStatus() {
    }

    public LEDFrameStatus(LEDFrame value) {
        this.value = value;
    }
}
