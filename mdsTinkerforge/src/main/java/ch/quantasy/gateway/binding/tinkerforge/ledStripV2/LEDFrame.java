/*
 * /*
 *  *   "TiMqWay"
 *  *
 *  *    TiMqWay(tm): A gateway to provide an MQTT-View for the Tinkerforge(tm) world (Tinkerforge-MQTT-Gateway).
 *  *
 *  *    Copyright (c) 2016 Bern University of Applied Sciences (BFH),
 *  *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *    Quellgasse 21, CH-2501 Biel, Switzerland
 *  *
 *  *    Licensed under Dual License consisting of:
 *  *    1. GNU Affero General Public License (AGPL) v3
 *  *    and
 *  *    2. Commercial license
 *  *
 *  *
 *  *    1. This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU Affero General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU Affero General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU Affero General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  *
 *  *
 *  *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *  *     accordance with the commercial license agreement provided with the
 *  *     Software or, alternatively, in accordance with the terms contained in
 *  *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *  *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *  *
 *  *
 *  *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *  *
 *  *
 */
package ch.quantasy.gateway.binding.tinkerforge.ledStripV2;

import ch.quantasy.mdservice.message.annotations.AValidator;
import ch.quantasy.mdservice.message.annotations.ArraySize;
import ch.quantasy.mdservice.message.annotations.MultiArraySize;
import ch.quantasy.mdservice.message.annotations.NonNull;
import ch.quantasy.mdservice.message.annotations.Nullable;
import ch.quantasy.mdservice.message.annotations.Range;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author reto
 */
public class LEDFrame extends AValidator {

    @NonNull
    @MultiArraySize(values = {
        @ArraySize(min = 1, max = 4),
        @ArraySize(min = 1, max = 2048)})
    //@Range(from = 0, to = 255)
    private int[][] leds;

    @Nullable
    @Range(from = 0, to = Integer.MAX_VALUE)
    public Integer durationInMillis;

    private LEDFrame() {
    }

    public LEDFrame(int amountOfChannels, int amountOfLEDs, Integer durationInMillis) {
        this.leds = new int[amountOfChannels][amountOfLEDs];
        this.durationInMillis = durationInMillis;
    }

    public LEDFrame(LEDFrame frame) {
        this(frame.leds, frame.durationInMillis);
    }

    public LEDFrame(Set<LEDValue> ledValues, LEDStripDeviceConfig config) {
        this(config.chipType.numberOfChannels, config.numberOfLEDs, config.frameDurationInMilliseconds);
        setColor(ledValues);
    }

    public LEDFrame(int[][] leds, Integer durationInMillis) {
        this.durationInMillis = durationInMillis;
        this.leds = new int[leds.length][];
        this.leds[0] = leds[0].clone();

        for (int i = 1; i < this.leds.length; i++) {
            if (leds[i - 1].length != leds[i].length) {
                throw new IllegalArgumentException();
            }
            this.leds[i] = leds[i].clone();
        }
    }

    public int getMaxValue() {
        int maxValue = Integer.MIN_VALUE;
        for (int i = 0; i < leds.length; i++) {
            for (int j = 0; j < leds[i].length; j++) {
                if (maxValue < leds[i][j]) {
                    maxValue = leds[i][j];
                }
            }
        }
        return maxValue;
    }

    public int getMinValue() {
        int minValue = Integer.MAX_VALUE;
        for (int i = 0; i < leds.length; i++) {
            for (int j = 0; j < leds[i].length; j++) {
                if (minValue > leds[i][j]) {
                    minValue = leds[i][j];
                }
            }
        }
        return minValue;
    }

    public int getNumberOfChannels() {
        return leds.length;
    }

    public int getNumberOfLEDs() {
        return leds[0].length;
    }

    public int getColor(int channel, int position) {
        return leds[channel][position];
    }

    public int[] getColors() {
        return getColors(null);
    }

    public int[] getColors(int[] colors) {
        if (colors == null || colors.length != getNumberOfChannels() * getNumberOfLEDs()) {
            colors = new int[getNumberOfChannels() * getNumberOfLEDs()];
        }
        for (int channel = 0; channel < leds.length; channel++) {
            for (int position = 0; position < leds[channel].length; position++) {
                colors[position * getNumberOfChannels() + channel] = leds[channel][position];
            }
        }
        return colors;
    }

    public void setColor(int channel, int position, int color) {
        leds[channel][position] = color;
    }

    public void setColor(int pos, long color) {
        for (int i = 0; i < leds.length; i++) {
            leds[i][pos] = (short) ((color >> (24 - (i * 8))) & 0xFF);
        }
    }
    
    public void setColor(Set<LEDValue> ledValues){
        for (LEDValue value : ledValues) {
            setColor(value.pos, Long.parseLong(value.rgbw, 16));
        }
    }
 

    /**
     * Returns a new LEDFrame with all input-Frames combined (Max-Value)...
     *
     * @param ledFrames
     * @return
     */
    public LEDFrame combine(LEDFrame... ledFrames) {
        if (ledFrames == null || ledFrames.length == 0) {
            return this;
        }
        Integer durationInMillis = this.durationInMillis;
        int[][] leds = new int[this.leds.length][];

        for (LEDFrame ledFrame : ledFrames) {
            if (ledFrame.getNumberOfChannels() != this.getNumberOfChannels()) {
                continue;
            }
            if (ledFrame.getNumberOfLEDs() != this.getNumberOfLEDs()) {
                continue;
            }
            if (ledFrame.durationInMillis != null && ledFrame.durationInMillis < durationInMillis) {
                durationInMillis = ledFrame.durationInMillis;
            }
            for (int c = 0; c < getNumberOfChannels(); c++) {
                leds[c] = this.leds[c].clone();
                for (int pos = 0; pos < getNumberOfLEDs(); pos++) {
                    leds[c][pos] = (short) Math.max(leds[c][pos], ledFrame.leds[c][pos]);
                }
            }
        }
        if (leds[0] == null) {
            return this;
        } else {
            LEDFrame frame = new LEDFrame(leds, durationInMillis);
            return frame;
        }
    }

    /**
     * Returns a new LEDFrame with the specified brightness. 0 is true black,
     * 255 is true white
     *
     * @param ledFrame
     * @param brightnessFactor (something between 0.0 and Infinity)
     */
    public LEDFrame(LEDFrame ledFrame, double brightnessFactor) {
        this(ledFrame);
        for (int i = 0; i < leds.length; i++) {
            for (int j = 0; j < leds[i].length; j++) {
                leds[i][j] = (short) Math.min(255, (short) ((leds[i][j] * brightnessFactor) + 0.5));
                leds[i][j] = (short) Math.max(0, leds[i][j]);
                if (leds[i][j] < 0 || leds[i][j] > 255) {
                    Logger.getLogger(LEDFrame.class.getName()).log(Level.SEVERE, "Illegal Argument in frame: " + leds[i][j]);
                }
            }
        }
    }
}
