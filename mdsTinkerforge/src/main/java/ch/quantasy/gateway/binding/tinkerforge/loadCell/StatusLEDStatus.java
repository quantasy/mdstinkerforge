package ch.quantasy.gateway.binding.tinkerforge.loadCell;

import java.lang.Boolean;
import ch.quantasy.mdservice.message.AStatus;

public class StatusLEDStatus extends AStatus {

    public Boolean value;

    private StatusLEDStatus() {
    }

    public StatusLEDStatus(Boolean value) {
        this.value = value;
    }
}
