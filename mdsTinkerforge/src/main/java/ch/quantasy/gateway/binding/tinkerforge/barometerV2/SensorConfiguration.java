/*
 * /*
 *  *   "TiMqWay"
 *  *
 *  *    TiMqWay(tm): A gateway to provide an MQTT-View for the Tinkerforge(tm) world (Tinkerforge-MQTT-Gateway).
 *  *
 *  *    Copyright (c) 2016 Bern University of Applied Sciences (BFH),
 *  *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *    Quellgasse 21, CH-2501 Biel, Switzerland
 *  *
 *  *    Licensed under Dual License consisting of:
 *  *    1. GNU Affero General Public License (AGPL) v3
 *  *    and
 *  *    2. Commercial license
 *  *
 *  *
 *  *    1. This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU Affero General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU Affero General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU Affero General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  *
 *  *
 *  *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *  *     accordance with the commercial license agreement provided with the
 *  *     Software or, alternatively, in accordance with the terms contained in
 *  *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *  *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *  *
 *  *
 *  *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *  *
 *  *
 */
package ch.quantasy.gateway.binding.tinkerforge.barometerV2;

import ch.quantasy.mdservice.message.annotations.AValidator;
import ch.quantasy.mdservice.message.annotations.NonNull;
import com.tinkerforge.BrickletBarometerV2;

/**
 *
 * @author reto
 */
public class SensorConfiguration extends AValidator {
    
    public static enum AirPressureLowPassFilter {
        LOW_PASS_OFF(0),LOW_PASS_1_9TH(1),LOW_PASS_1_20TH(2);
        
        private int value;

        private AirPressureLowPassFilter(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static AirPressureLowPassFilter getAirPressureLowPassFilterFor(int s) throws IllegalArgumentException {
            for (AirPressureLowPassFilter range : values()) {
                if (range.value == s) {
                    return range;
                }
            }
            throw new IllegalArgumentException();
        }
    }
    public static enum DataRate {
        DATA_OFF(0),Hz1(1),Hz10(2),Hz25(3),Hz50(4),Hz75(5);
        
        private int value;

        private DataRate(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static DataRate getDataRateFor(int s) throws IllegalArgumentException {
            for (DataRate range : values()) {
                if (range.value == s) {
                    return range;
                }
            }
            throw new IllegalArgumentException();
        }
    }
    @NonNull
    public AirPressureLowPassFilter airPressureLowPassFilter;
    @NonNull
    public DataRate dataRate;
  
    private SensorConfiguration() {
    }

    public SensorConfiguration(AirPressureLowPassFilter airPressureLowPassFilter, DataRate dataRate) {
        this.airPressureLowPassFilter = airPressureLowPassFilter;
        this.dataRate = dataRate;
    }
    
    public SensorConfiguration(BrickletBarometerV2.SensorConfiguration configuration){
        this.airPressureLowPassFilter=AirPressureLowPassFilter.getAirPressureLowPassFilterFor(configuration.airPressureLowPassFilter);
        this.dataRate=DataRate.getDataRateFor(configuration.dataRate);
    }
    

   

}
