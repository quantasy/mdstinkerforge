package ch.quantasy.gateway.binding.tinkerforge.master;
import java.lang.Long;
import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;
public class UsbVoltageCallbackPeriodStatus extends AStatus{
@Period
public Long value;
private UsbVoltageCallbackPeriodStatus(){}
public UsbVoltageCallbackPeriodStatus(Long value){
  this.value=value;
}
}
