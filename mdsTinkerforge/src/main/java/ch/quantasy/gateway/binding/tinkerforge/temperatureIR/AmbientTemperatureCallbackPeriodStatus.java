package ch.quantasy.gateway.binding.tinkerforge.temperatureIR;
import java.lang.Long;
import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;
public class AmbientTemperatureCallbackPeriodStatus extends AStatus{
@Period
public Long value;
private AmbientTemperatureCallbackPeriodStatus(){}
public AmbientTemperatureCallbackPeriodStatus(Long value){
  this.value=value;
}
}
