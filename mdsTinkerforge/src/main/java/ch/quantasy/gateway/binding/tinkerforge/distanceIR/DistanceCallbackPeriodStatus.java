package ch.quantasy.gateway.binding.tinkerforge.distanceIR;

import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;

public class DistanceCallbackPeriodStatus extends AStatus {

    @Period
    public Long value;

    private DistanceCallbackPeriodStatus() {
    }

    public DistanceCallbackPeriodStatus(Long value) {
        this.value = value;
    }
}
