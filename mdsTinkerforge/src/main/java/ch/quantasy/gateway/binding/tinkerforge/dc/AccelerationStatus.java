package ch.quantasy.gateway.binding.tinkerforge.dc;

import ch.quantasy.mdservice.message.annotations.Range;
import ch.quantasy.mdservice.message.AStatus;

public class AccelerationStatus extends AStatus {

    @Range(from = 0, to = 2147483647)
    public Integer value;

    private AccelerationStatus() {
    }

    public AccelerationStatus(Integer value) {
        this.value = value;
    }
}
