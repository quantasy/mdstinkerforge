package ch.quantasy.gateway.binding.tinkerforge.moisture;
import java.lang.Short;
import ch.quantasy.mdservice.message.annotations.Range;
import ch.quantasy.mdservice.message.AStatus;
public class MovingAverageStatus extends AStatus{
@Range(from=0, to=100)
public Short value;
private MovingAverageStatus(){}
public MovingAverageStatus(Short value){
  this.value=value;
}
}
