package ch.quantasy.gateway.binding.tinkerforge.laserRangeFinder;

import ch.quantasy.mdservice.message.AStatus;

public class DistanceCallbackThresholdStatus extends AStatus {

    public DeviceDistanceCallbackThreshold value;

    private DistanceCallbackThresholdStatus() {
    }

    public DistanceCallbackThresholdStatus(DeviceDistanceCallbackThreshold value) {
        this.value = value;
    }
}
