package ch.quantasy.gateway.binding.tinkerforge.IMUV2;

import ch.quantasy.mdservice.message.annotations.Period;

import ch.quantasy.mdservice.message.AStatus;

public class TemperatureCallbackPeriodStatus extends AStatus {

    @Period
    public long value;

    private TemperatureCallbackPeriodStatus() {
    }

    public TemperatureCallbackPeriodStatus(long value) {
        this.value = value;
    }
}
