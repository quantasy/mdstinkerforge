package ch.quantasy.gateway.binding.tinkerforge.dc;

import ch.quantasy.mdservice.message.annotations.Range;
import ch.quantasy.mdservice.message.AStatus;

public class MinimumVoltageStatus extends AStatus {

    @Range(from = 6000, to = 2147483647)
    public Integer value;

    private MinimumVoltageStatus() {
    }

    public MinimumVoltageStatus(Integer value) {
        this.value = value;
    }
}
