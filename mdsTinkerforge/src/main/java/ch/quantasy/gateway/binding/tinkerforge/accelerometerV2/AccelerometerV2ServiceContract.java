/*
 * /*
 *  *   "TiMqWay"
 *  *
 *  *    TiMqWay(tm): A gateway to provide an MQTT-View for the Tinkerforge(tm) world (Tinkerforge-MQTT-Gateway).
 *  *
 *  *    Copyright (c) 2016 Bern University of Applied Sciences (BFH),
 *  *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *    Quellgasse 21, CH-2501 Biel, Switzerland
 *  *
 *  *    Licensed under Dual License consisting of:
 *  *    1. GNU Affero General Public License (AGPL) v3
 *  *    and
 *  *    2. Commercial license
 *  *
 *  *
 *  *    1. This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU Affero General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU Affero General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU Affero General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  *
 *  *
 *  *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *  *     accordance with the commercial license agreement provided with the
 *  *     Software or, alternatively, in accordance with the terms contained in
 *  *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *  *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *  *
 *  *
 *  *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *  *
 *  *
 */
package ch.quantasy.gateway.binding.tinkerforge.accelerometerV2;

import ch.quantasy.gateway.binding.tinkerforge.DeviceServiceContract;
import ch.quantasy.mdservice.message.Message;
import ch.quantasy.tinkerforge.device.TinkerforgeDeviceClass;
import ch.quantasy.tinkerforge.device.accelerometerV2.AccelerometerV2Device;
import java.util.Map;

/**
 *
 * @author reto
 */
public class AccelerometerV2ServiceContract extends DeviceServiceContract {

    public final String ACCELERATION;
    public final String STATUS_ACCELERATION;
    public final String EVENT_ACCELERATION;
    public final String EVENT_CONTINUOUS_ACCELERATION_BIT16;
    public final String EVENT_CONTINUOUS_ACCELERATION_BIT8;

    public final String STATUS_DEVICE_CONFIGURATION;
    public final String STATUS_CONTINUOUS_ACCELERATION_CONFIGURATION;
    public final String STATUS_ACCELERATION_CALLBACK_CONFIGURATION;

    public AccelerometerV2ServiceContract(AccelerometerV2Device device) {
        this(device.getUid(), TinkerforgeDeviceClass.getDevice(device.getDevice()).toString());
    }

    public AccelerometerV2ServiceContract(String id) {
        this(id, TinkerforgeDeviceClass.AccelerometerV2.toString());
    }

    public AccelerometerV2ServiceContract(String id, String device) {
        super(id, device, AccelerometerV2Intent.class);

        ACCELERATION = "acceleration";
        STATUS_ACCELERATION = STATUS + "/" + ACCELERATION;
        STATUS_ACCELERATION_CALLBACK_CONFIGURATION = STATUS_ACCELERATION + "/" + "callbackConfiguration";
        EVENT_ACCELERATION = EVENT + "/" + ACCELERATION;
        EVENT_CONTINUOUS_ACCELERATION_BIT16 = EVENT + "/" + "continuous" + "/" + ACCELERATION + "/" + "16bit";
        EVENT_CONTINUOUS_ACCELERATION_BIT8 = EVENT + "/" + "continuous" + "/" + ACCELERATION + "/" + "8bit";

        STATUS_DEVICE_CONFIGURATION = STATUS + "/" + "deviceConfiguration";
        STATUS_CONTINUOUS_ACCELERATION_CONFIGURATION = STATUS + "/" + "continuousAccelerationConfiguration";
    }

    @Override
    public void setServiceMessageTopics(Map<String, Class<? extends Message>> messageTopicMap) {
        messageTopicMap.put(EVENT_ACCELERATION, AccelerometerEvent.class);
        messageTopicMap.put(EVENT_ACCELERATION, AccelerometerEvent.class);
        messageTopicMap.put(EVENT_CONTINUOUS_ACCELERATION_BIT16, ContinuousAccelerationEvent.class);
        messageTopicMap.put(EVENT_CONTINUOUS_ACCELERATION_BIT8, ContinuousAccelerationEvent.class);
        messageTopicMap.put(STATUS_ACCELERATION_CALLBACK_CONFIGURATION, AccelerationCallbackConfigurationStatus.class);
        messageTopicMap.put(STATUS_CONTINUOUS_ACCELERATION_CONFIGURATION, ContinuousAccelerationCallbackConfigurationStatus.class);

        messageTopicMap.put(STATUS_DEVICE_CONFIGURATION, DeviceConfigurationStatus.class);
    }

    public static void main(String[] args) {
        AccelerometerV2ServiceContract c = new AccelerometerV2ServiceContract("<id>");
        System.out.println(c.toMD());
    }
}
