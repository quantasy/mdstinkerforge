package ch.quantasy.gateway.binding.tinkerforge.dustDetector;

import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;

public class DustDensityCallbackPeriodStatus extends AStatus {

    @Period
    public Long value;

    private DustDensityCallbackPeriodStatus() {
    }

    public DustDensityCallbackPeriodStatus(Long value) {
        this.value = value;
    }
}
