package ch.quantasy.gateway.binding.tinkerforge.dc;

import ch.quantasy.mdservice.message.annotations.Choice;

import ch.quantasy.mdservice.message.AStatus;

public class DriveModeStatus extends AStatus {

    @Choice(values = {"1", "2"})
    public Short value;

    private DriveModeStatus() {
    }

    public DriveModeStatus(Short value) {
        this.value = value;
    }
}
