package ch.quantasy.gateway.binding.tinkerforge.IMUV2;

import ch.quantasy.mdservice.message.annotations.Period;

import ch.quantasy.mdservice.message.AStatus;

public class GravityVectorCallbackPeriodStatus extends AStatus {

    @Period
    public Long value;

    private GravityVectorCallbackPeriodStatus() {
    }

    public GravityVectorCallbackPeriodStatus(Long value) {
        this.value = value;
    }
}
