package ch.quantasy.gateway.binding.tinkerforge.hallEffect;

import ch.quantasy.mdservice.message.AStatus;

public class EdgeCountResetStatus extends AStatus {

    public Boolean value;

    private EdgeCountResetStatus() {
    }

    public EdgeCountResetStatus(Boolean value) {
        this.value = value;
    }
}
