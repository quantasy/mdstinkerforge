package ch.quantasy.gateway.binding.tinkerforge.realTimeClock;
import java.lang.Byte;
import ch.quantasy.mdservice.message.annotations.Range;
import ch.quantasy.mdservice.message.AStatus;
public class OffsetStatus extends AStatus{
@Range(from=-128, to=127)
public Byte value;
private OffsetStatus(){}
public OffsetStatus(Byte value){
  this.value=value;
}
}
