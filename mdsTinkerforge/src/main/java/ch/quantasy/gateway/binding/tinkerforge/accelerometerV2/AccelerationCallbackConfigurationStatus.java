package ch.quantasy.gateway.binding.tinkerforge.accelerometerV2;

import ch.quantasy.mdservice.message.AStatus;

public class AccelerationCallbackConfigurationStatus extends AStatus {

    public AccelerationCallbackConfiguration value;

    private AccelerationCallbackConfigurationStatus() {
    }

    public AccelerationCallbackConfigurationStatus(AccelerationCallbackConfiguration value) {
        this.value = value;
    }
}
