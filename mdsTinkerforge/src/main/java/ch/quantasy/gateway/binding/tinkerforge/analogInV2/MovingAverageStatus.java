package ch.quantasy.gateway.binding.tinkerforge.analogInV2;

import java.lang.Short;
import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;

public class MovingAverageStatus extends AStatus {

    @Period
    public Short value;

    private MovingAverageStatus() {
    }

    public MovingAverageStatus(Short value) {
        this.value = value;
    }
}
