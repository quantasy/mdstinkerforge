package ch.quantasy.gateway.binding.tinkerforge.LCD16x2;

import ch.quantasy.mdservice.message.AStatus;

public class ClearDisplayStatus extends AStatus {

    public boolean value;

    private ClearDisplayStatus() {
    }

    public ClearDisplayStatus(boolean value) {
        this.value = value;
    }
}
