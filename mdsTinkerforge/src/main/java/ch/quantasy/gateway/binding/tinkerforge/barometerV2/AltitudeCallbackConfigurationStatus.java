package ch.quantasy.gateway.binding.tinkerforge.barometerV2;

import ch.quantasy.mdservice.message.AStatus;
import ch.quantasy.mdservice.message.annotations.NonNull;

public class AltitudeCallbackConfigurationStatus extends AStatus {

    @NonNull
    public DeviceAltitudeCallbackConfiguration value;

    private AltitudeCallbackConfigurationStatus() {
    }

    public AltitudeCallbackConfigurationStatus(DeviceAltitudeCallbackConfiguration value) {
        this.value = value;
    }
}
