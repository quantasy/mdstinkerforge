package ch.quantasy.gateway.binding.tinkerforge.accelerometerV2;

import ch.quantasy.mdservice.message.AStatus;

public class ContinuousAccelerationCallbackConfigurationStatus extends AStatus {

    public ContinuousAccelerationConfiguration value;

    private ContinuousAccelerationCallbackConfigurationStatus() {
    }

    public ContinuousAccelerationCallbackConfigurationStatus(ContinuousAccelerationConfiguration value) {
        this.value = value;
    }
}
