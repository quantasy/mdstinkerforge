package ch.quantasy.gateway.binding.tinkerforge.laserRangeFinder;

import ch.quantasy.mdservice.message.AStatus;

public class SensorHardwareStatus extends AStatus {

    public SensorHardware value;

    private SensorHardwareStatus() {
    }

    public SensorHardwareStatus(SensorHardware value) {
        this.value = value;
    }
}
