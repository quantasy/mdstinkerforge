package ch.quantasy.gateway.binding.tinkerforge.humidity;

import java.lang.Long;
import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;

public class HumidityCallbackPeriodStatus extends AStatus {

    @Period
    public Long value;

    private HumidityCallbackPeriodStatus() {
    }

    public HumidityCallbackPeriodStatus(Long value) {
        this.value = value;
    }
}
