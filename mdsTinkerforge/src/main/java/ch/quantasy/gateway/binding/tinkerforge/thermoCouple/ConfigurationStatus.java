package ch.quantasy.gateway.binding.tinkerforge.thermoCouple;

import ch.quantasy.mdservice.message.AStatus;

public class ConfigurationStatus extends AStatus {

    public DeviceConfiguration value;

    private ConfigurationStatus() {
    }

    public ConfigurationStatus(DeviceConfiguration value) {
        this.value = value;
    }
}
