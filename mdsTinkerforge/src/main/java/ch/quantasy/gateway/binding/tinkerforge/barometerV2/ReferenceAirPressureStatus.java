package ch.quantasy.gateway.binding.tinkerforge.barometerV2;

import ch.quantasy.mdservice.message.AStatus;
import ch.quantasy.mdservice.message.annotations.Range;

public class ReferenceAirPressureStatus extends AStatus {

    @Range(from = 260000 , to = 1260000)
    public int value;

    private ReferenceAirPressureStatus() {
    }

    public ReferenceAirPressureStatus(int value) {
        this.value = value;
    }
}
