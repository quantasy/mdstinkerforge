package ch.quantasy.gateway.binding.tinkerforge.ledStripV2;

import ch.quantasy.mdservice.message.annotations.ArraySize;

import ch.quantasy.mdservice.message.AStatus;

public class LEDFramesStatus extends AStatus {

    @ArraySize(max = 2147483647, min = 1)
    public LEDFrame[] value;

    private LEDFramesStatus() {
    }

    public LEDFramesStatus(LEDFrame[] value) {
        this.value = value;
    }
}
