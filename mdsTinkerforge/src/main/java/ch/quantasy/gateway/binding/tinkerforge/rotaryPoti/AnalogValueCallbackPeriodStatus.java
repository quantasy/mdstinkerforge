package ch.quantasy.gateway.binding.tinkerforge.rotaryPoti;
import java.lang.Long;
import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;
public class AnalogValueCallbackPeriodStatus extends AStatus{
@Period
public Long value;
private AnalogValueCallbackPeriodStatus(){}
public AnalogValueCallbackPeriodStatus(Long value){
  this.value=value;
}
}
