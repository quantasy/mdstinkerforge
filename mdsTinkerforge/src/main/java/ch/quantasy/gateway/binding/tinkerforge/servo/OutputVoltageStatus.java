package ch.quantasy.gateway.binding.tinkerforge.servo;
import java.lang.Integer;
import ch.quantasy.mdservice.message.annotations.Range;
import ch.quantasy.mdservice.message.AStatus;
public class OutputVoltageStatus extends AStatus{
@Range(from=2000, to=9000)
public Integer value;
private OutputVoltageStatus(){}
public OutputVoltageStatus(Integer value){
  this.value=value;
}
}
