package ch.quantasy.gateway.binding.tinkerforge.barometerV2;

import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;

public class CalibrationStatus extends AStatus {

    @Period
    public Calibration value;

    private CalibrationStatus() {
    }

    public CalibrationStatus(Calibration value) {
        this.value = value;
    }
}
