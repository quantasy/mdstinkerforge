package ch.quantasy.gateway.binding.tinkerforge.IMU;

import ch.quantasy.mdservice.message.annotations.Period;

import ch.quantasy.mdservice.message.AStatus;

public class OrientationCallbackPeriodStatus extends AStatus {

    @Period
    public Long value;

    private OrientationCallbackPeriodStatus() {
    }

    public OrientationCallbackPeriodStatus(Long value) {
        this.value = value;
    }
}
