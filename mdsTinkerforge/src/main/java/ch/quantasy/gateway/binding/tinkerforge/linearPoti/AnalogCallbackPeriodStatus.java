package ch.quantasy.gateway.binding.tinkerforge.linearPoti;

import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;

public class AnalogCallbackPeriodStatus extends AStatus {

    @Period
    public Long value;

    private AnalogCallbackPeriodStatus() {
    }

    public AnalogCallbackPeriodStatus(Long value) {
        this.value = value;
    }
}
