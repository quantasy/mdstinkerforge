package ch.quantasy.gateway.binding.tinkerforge.voltageCurrent;
import java.lang.Long;
import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;
public class VoltageCallbackPeriodStatus extends AStatus{
@Period
public Long value;
private VoltageCallbackPeriodStatus(){}
public VoltageCallbackPeriodStatus(Long value){
  this.value=value;
}
}
