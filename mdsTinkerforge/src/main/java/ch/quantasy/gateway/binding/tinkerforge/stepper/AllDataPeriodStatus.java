package ch.quantasy.gateway.binding.tinkerforge.stepper;
import java.lang.Integer;
import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;
public class AllDataPeriodStatus extends AStatus{
@Period
public Long value;
private AllDataPeriodStatus(){}
public AllDataPeriodStatus(Long value){
  this.value=value;
}
}
