/*
 * /*
 *  *   "TiMqWay"
 *  *
 *  *    TiMqWay(tm): A gateway to provide an MQTT-View for the Tinkerforge(tm) world (Tinkerforge-MQTT-Gateway).
 *  *
 *  *    Copyright (c) 2016 Bern University of Applied Sciences (BFH),
 *  *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *    Quellgasse 21, CH-2501 Biel, Switzerland
 *  *
 *  *    Licensed under Dual License consisting of:
 *  *    1. GNU Affero General Public License (AGPL) v3
 *  *    and
 *  *    2. Commercial license
 *  *
 *  *
 *  *    1. This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU Affero General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU Affero General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU Affero General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  *
 *  *
 *  *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *  *     accordance with the commercial license agreement provided with the
 *  *     Software or, alternatively, in accordance with the terms contained in
 *  *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *  *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *  *
 *  *
 *  *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *  *
 *  *
 */
package ch.quantasy.gateway.binding.tinkerforge.accelerometerV2;

import ch.quantasy.mdservice.message.annotations.AValidator;
import ch.quantasy.mdservice.message.annotations.NonNull;
import com.tinkerforge.BrickletAccelerometerV2;

/**
 *
 * @author reto
 */
public class DeviceConfiguration extends AValidator {

    public static enum DataRate {
        Hz781(0), Hz563( 1), Hz125( 2), Hz2612( 3), Hz5(4), Hz25( 5), Hz50(6), Hz100( 7), Hz200(8), Hz400(9),Hz800(10),Hz1600(11),Hz3200(12),Hz6400(13),Hz12800(14),Hz25600(15);
        private int value;

        private DataRate(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static DataRate getDataRateFor(int s) throws IllegalArgumentException {
            for (DataRate range : values()) {
                if (range.value == s) {
                    return range;
                }
            }
            throw new IllegalArgumentException("Not supported: " + s);
        }
    }

    public static enum FullScale {
        G2( 0), G4( 1), G8( 2);
        private int value;

        private FullScale(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static FullScale getFullScaleFor(int s) throws IllegalArgumentException {
            for (FullScale range : values()) {
                if (range.value == s) {
                    return range;
                }
            }
            throw new IllegalArgumentException();
        }
    }

   
    @NonNull
    public DataRate dataRate;
    @NonNull
    public FullScale fullScale;
   
  
    private DeviceConfiguration() {
    }

    public DeviceConfiguration(String dataRate, String fullScale) {
        this(DataRate.valueOf(dataRate), FullScale.valueOf(fullScale));
    }

    public DeviceConfiguration(DataRate dataRate, FullScale fullScale) {
        this.dataRate = dataRate;
        this.fullScale = fullScale;
        
    }

    public DeviceConfiguration(int dataRate, int fullScale) throws IllegalArgumentException {
        this(DataRate.getDataRateFor(dataRate), FullScale.getFullScaleFor(fullScale));
    }

    public DeviceConfiguration(BrickletAccelerometerV2.Configuration configuration) {
        this(configuration.dataRate, configuration.fullScale);
    }

}
