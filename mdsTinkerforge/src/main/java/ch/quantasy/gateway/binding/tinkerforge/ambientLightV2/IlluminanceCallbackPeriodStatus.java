package ch.quantasy.gateway.binding.tinkerforge.ambientLightV2;

import java.lang.Long;
import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;

public class IlluminanceCallbackPeriodStatus extends AStatus {

    @Period
    public Long value;

    private IlluminanceCallbackPeriodStatus() {
    }

    public IlluminanceCallbackPeriodStatus(Long value) {
        this.value = value;
    }
}
