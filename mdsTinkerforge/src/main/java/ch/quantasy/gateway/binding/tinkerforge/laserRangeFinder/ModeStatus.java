package ch.quantasy.gateway.binding.tinkerforge.laserRangeFinder;

import ch.quantasy.mdservice.message.AStatus;

public class ModeStatus extends AStatus {

    public DeviceMode value;

    private ModeStatus() {
    }

    public ModeStatus(DeviceMode value) {
        this.value = value;
    }
}
