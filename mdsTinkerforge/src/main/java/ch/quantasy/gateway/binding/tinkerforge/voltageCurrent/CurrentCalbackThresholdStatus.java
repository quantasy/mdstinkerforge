package ch.quantasy.gateway.binding.tinkerforge.voltageCurrent;
import ch.quantasy.gateway.binding.tinkerforge.voltageCurrent.DeviceCurrentCallbackThreshold;
import ch.quantasy.mdservice.message.AStatus;
public class CurrentCalbackThresholdStatus extends AStatus{
public DeviceCurrentCallbackThreshold value;
private CurrentCalbackThresholdStatus(){}
public CurrentCalbackThresholdStatus(DeviceCurrentCallbackThreshold value){
  this.value=value;
}
}
