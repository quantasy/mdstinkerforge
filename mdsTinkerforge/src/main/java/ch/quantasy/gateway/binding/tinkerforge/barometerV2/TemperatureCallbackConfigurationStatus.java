package ch.quantasy.gateway.binding.tinkerforge.barometerV2;

import ch.quantasy.mdservice.message.AStatus;
import ch.quantasy.mdservice.message.annotations.NonNull;

public class TemperatureCallbackConfigurationStatus extends AStatus {

    @NonNull
    public DeviceTemperatureCallbackConfiguration value;

    private TemperatureCallbackConfigurationStatus() {
    }

    public TemperatureCallbackConfigurationStatus(DeviceTemperatureCallbackConfiguration value) {
        this.value = value;
    }
}
