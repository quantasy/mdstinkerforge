package ch.quantasy.gateway.binding.tinkerforge.remoteSwitch;

import ch.quantasy.mdservice.message.AStatus;

public class SwitchSocketAParametersStatus extends AStatus {

    public SwitchSocketAParameters value;

    private SwitchSocketAParametersStatus() {
    }

    public SwitchSocketAParametersStatus(SwitchSocketAParameters value) {
        this.value = value;
    }
}
