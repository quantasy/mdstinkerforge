package ch.quantasy.gateway.binding.tinkerforge.barometerV2;

import ch.quantasy.mdservice.message.AStatus;
import ch.quantasy.mdservice.message.annotations.NonNull;

public class SensorConfigurationStatus extends AStatus {

    @NonNull
    public SensorConfiguration value;

    private SensorConfigurationStatus() {
    }

    public SensorConfigurationStatus(SensorConfiguration value) {
        this.value = value;
    }
}
