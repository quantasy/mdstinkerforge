package ch.quantasy.gateway.binding.tinkerforge.remoteSwitch;

import ch.quantasy.mdservice.message.annotations.Range;
import ch.quantasy.mdservice.message.AStatus;

public class RepeatsStatus extends AStatus {

    @Range(from = 0, to = 32767)
    public short value;

    private RepeatsStatus() {
    }

    public RepeatsStatus(short value) {
        this.value = value;
    }
}
