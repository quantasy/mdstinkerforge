package ch.quantasy.gateway.binding.tinkerforge.nfc;
import java.lang.Long;
import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;
public class ScanningIntervalStatus extends AStatus{
@Period
public Long value;
private ScanningIntervalStatus(){}
public ScanningIntervalStatus(Long value){
  this.value=value;
}
}
