package ch.quantasy.gateway.binding.tinkerforge.laserRangeFinder;

import ch.quantasy.mdservice.message.AStatus;

public class VelocityCallbackThresholdStatus extends AStatus {

    public DeviceVelocityCallbackThreshold value;

    private VelocityCallbackThresholdStatus() {
    }

    public VelocityCallbackThresholdStatus(DeviceVelocityCallbackThreshold value) {
        this.value = value;
    }
}
