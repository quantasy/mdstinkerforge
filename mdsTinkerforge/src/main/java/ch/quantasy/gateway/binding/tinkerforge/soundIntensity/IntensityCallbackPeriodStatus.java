package ch.quantasy.gateway.binding.tinkerforge.soundIntensity;
import ch.quantasy.mdservice.message.AStatus;
import ch.quantasy.mdservice.message.annotations.Period;
import java.lang.Long;

public class IntensityCallbackPeriodStatus extends AStatus{
@Period
public Long value;
private IntensityCallbackPeriodStatus(){}
public IntensityCallbackPeriodStatus(Long value){
  this.value=value;
}
}
