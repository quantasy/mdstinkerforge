package ch.quantasy.gateway.binding.tinkerforge.laserRangeFinder;

import java.lang.Long;
import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;

public class VelocityCallbackPeriodStatus extends AStatus {

    @Period
    public Long value;

    private VelocityCallbackPeriodStatus() {
    }

    public VelocityCallbackPeriodStatus(Long value) {
        this.value = value;
    }
}
