package ch.quantasy.gateway.binding.tinkerforge.distanceIR;

import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;

public class DebouncePeriodStatus extends AStatus {

    @Period
    public Long value;

    private DebouncePeriodStatus() {
    }

    public DebouncePeriodStatus(Long value) {
        this.value = value;
    }
}
