package ch.quantasy.gateway.binding.tinkerforge.loadCell;

import ch.quantasy.mdservice.message.AStatus;

public class CalibrateStatus extends AStatus {

    public Long value;

    private CalibrateStatus() {
    }

    public CalibrateStatus(Long value) {
        this.value = value;
    }
}
