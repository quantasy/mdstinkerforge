/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.quantasy.gateway.binding.tinkerforge.CoProcessor;

import ch.quantasy.mdservice.message.AStatus;
import ch.quantasy.mdservice.message.annotations.NonNull;
import ch.quantasy.mdservice.message.annotations.Range;

/**
 *
 * @author reto
 */
public class SPITFPErrorStatus extends AStatus {

    @Range(from = 0)
    @NonNull
    public long errorCountAckChecksum;
    @Range(from = 0)
    @NonNull
    public long errorCountMessageChecksum;
    @Range(from = 0)
    @NonNull
    public long errorCountFrame;
    @Range(from = 0)
    @NonNull
    public long errorCountOverflow;

    private SPITFPErrorStatus() {
    }

    public SPITFPErrorStatus(long errorCountAckChecksum, long errorCountMessageChecksum, long errorCountFrame, long errorCountOverflow) {
        this.errorCountAckChecksum = errorCountAckChecksum;
        this.errorCountMessageChecksum = errorCountMessageChecksum;
        this.errorCountFrame = errorCountFrame;
        this.errorCountOverflow = errorCountOverflow;
    }

}
