package ch.quantasy.gateway.binding.tinkerforge.nfc;
import ch.quantasy.gateway.binding.tinkerforge.nfc.NFCWrite;
import ch.quantasy.mdservice.message.AStatus;
public class NfcWriteStatus extends AStatus{
public NFCWrite value;
private NfcWriteStatus(){}
public NfcWriteStatus(NFCWrite value){
  this.value=value;
}
}
