/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.quantasy.gateway.binding.tinkerforge.CoProcessor;

import ch.quantasy.mdservice.message.AnIntent;
import ch.quantasy.mdservice.message.Intent;
import ch.quantasy.mdservice.message.annotations.Nullable;

/**
 *
 * @author reto
 */
public class IntentCoProcessor extends AnIntent {

    @Nullable
    public Boolean spitfpErrorCount;

    @Nullable
    public StatusLEDConfig statusLEDConfig;

    @Nullable
    public Boolean reset;

    @Nullable
    public Boolean chipTemperature;

// public BrickletMotionDetectorV2.SPITFPErrorCount getSPITFPErrorCount();
// public int setBootloaderMode(int mode);
// public int getBootloaderMode();
// public void setWriteFirmwarePointer(long pointer);
// public int writeFirmware(int[] data);
// public void setStatusLEDConfig(int config);
// public int getStatusLEDConfig();
// public int getChipTemperature();
// public void reset();
// public void writeUID(long uid);
// public long readUID();
}
