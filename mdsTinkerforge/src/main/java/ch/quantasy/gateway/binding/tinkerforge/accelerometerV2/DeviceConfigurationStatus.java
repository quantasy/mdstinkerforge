package ch.quantasy.gateway.binding.tinkerforge.accelerometerV2;

import ch.quantasy.mdservice.message.AStatus;

public class DeviceConfigurationStatus extends AStatus {

    public DeviceConfiguration value;

    private DeviceConfigurationStatus() {
    }

    public DeviceConfigurationStatus(DeviceConfiguration value) {
        this.value = value;
    }
}
