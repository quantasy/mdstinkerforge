package ch.quantasy.gateway.binding.tinkerforge.temperatureIR;
import java.lang.Long;
import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;
public class ObjectTemperatureCallbackPeriodStatus extends AStatus{
@Period
public Long value;
private ObjectTemperatureCallbackPeriodStatus(){}
public ObjectTemperatureCallbackPeriodStatus(Long value){
  this.value=value;
}
}
