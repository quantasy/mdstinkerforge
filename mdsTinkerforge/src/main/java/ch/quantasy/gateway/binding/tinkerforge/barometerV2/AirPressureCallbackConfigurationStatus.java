package ch.quantasy.gateway.binding.tinkerforge.barometerV2;

import ch.quantasy.mdservice.message.AStatus;
import ch.quantasy.mdservice.message.annotations.NonNull;

public class AirPressureCallbackConfigurationStatus extends AStatus {

    @NonNull
    public DeviceAirPressureCallbackConfiguration value;

    private AirPressureCallbackConfigurationStatus() {
    }

    public AirPressureCallbackConfigurationStatus(DeviceAirPressureCallbackConfiguration value) {
        this.value = value;
    }
}
