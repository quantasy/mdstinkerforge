package ch.quantasy.gateway.binding.tinkerforge.device;

import ch.quantasy.mdservice.message.annotations.Range;
import ch.quantasy.mdservice.message.AStatus;
import ch.quantasy.mdservice.message.annotations.ArraySize;

public class Firmware extends AStatus {

    @ArraySize()
    @Range(from = Short.MIN_VALUE, to = Short.MAX_VALUE)
    public short[] value;

    private Firmware() {
    }

    public Firmware(short[] value) {
        this.value = value;
    }
}
