package ch.quantasy.gateway.binding.tinkerforge.IMU;

import ch.quantasy.mdservice.message.annotations.Period;

import ch.quantasy.mdservice.message.AStatus;

public class QuaternionCallbackPeriodStatus extends AStatus {

    @Period
    public Long value;

    private QuaternionCallbackPeriodStatus() {
    }

    public QuaternionCallbackPeriodStatus(Long value) {
        this.value = value;
    }
}
