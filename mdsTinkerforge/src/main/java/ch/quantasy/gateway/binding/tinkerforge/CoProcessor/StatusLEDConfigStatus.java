package ch.quantasy.gateway.binding.tinkerforge.CoProcessor;

import ch.quantasy.mdservice.message.AStatus;

public class StatusLEDConfigStatus extends AStatus {

    public StatusLEDConfig value;

    private StatusLEDConfigStatus() {
    }

    public StatusLEDConfigStatus(StatusLEDConfig value) {
        this.value = value;
    }
}
