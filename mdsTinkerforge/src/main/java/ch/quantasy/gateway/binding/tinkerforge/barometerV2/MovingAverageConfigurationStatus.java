package ch.quantasy.gateway.binding.tinkerforge.barometerV2;

import ch.quantasy.mdservice.message.AStatus;
import ch.quantasy.mdservice.message.annotations.NonNull;

public class MovingAverageConfigurationStatus extends AStatus {

    @NonNull
    public MovingAverageConfiguration value;

    private MovingAverageConfigurationStatus() {
    }

    public MovingAverageConfigurationStatus(MovingAverageConfiguration value) {
        this.value = value;
    }
}
