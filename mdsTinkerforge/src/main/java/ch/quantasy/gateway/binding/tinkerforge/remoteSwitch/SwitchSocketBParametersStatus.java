package ch.quantasy.gateway.binding.tinkerforge.remoteSwitch;

import ch.quantasy.mdservice.message.AStatus;

public class SwitchSocketBParametersStatus extends AStatus {

    public SwitchSocketBParameters value;

    private SwitchSocketBParametersStatus() {
    }

    public SwitchSocketBParametersStatus(SwitchSocketBParameters value) {
        this.value = value;
    }
}
