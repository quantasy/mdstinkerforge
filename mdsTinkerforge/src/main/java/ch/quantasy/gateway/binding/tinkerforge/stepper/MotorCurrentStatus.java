package ch.quantasy.gateway.binding.tinkerforge.stepper;

import ch.quantasy.mdservice.message.AStatus;
import ch.quantasy.mdservice.message.annotations.Range;

public class MotorCurrentStatus extends AStatus {

    @Range(from = 100, to = 2291)
    public Integer value;

    private MotorCurrentStatus() {
    }

    public MotorCurrentStatus(Integer value) {
        this.value = value;
    }
}
