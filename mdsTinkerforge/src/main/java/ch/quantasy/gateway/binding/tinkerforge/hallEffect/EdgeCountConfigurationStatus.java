package ch.quantasy.gateway.binding.tinkerforge.hallEffect;

import ch.quantasy.mdservice.message.AStatus;

public class EdgeCountConfigurationStatus extends AStatus {

    public DeviceConfiguration value;

    private EdgeCountConfigurationStatus() {
    }

    public EdgeCountConfigurationStatus(DeviceConfiguration value) {
        this.value = value;
    }
}
