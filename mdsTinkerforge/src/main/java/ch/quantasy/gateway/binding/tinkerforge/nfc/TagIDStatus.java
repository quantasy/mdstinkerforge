package ch.quantasy.gateway.binding.tinkerforge.nfc;
import java.lang.String;
import ch.quantasy.mdservice.message.annotations.StringForm;
import ch.quantasy.mdservice.message.AStatus;
public class TagIDStatus extends AStatus{
@StringForm(regEx="[0-9A-F]{8-14}")
public String value;
private TagIDStatus(){}
public TagIDStatus(String value){
  this.value=value;
}
}
