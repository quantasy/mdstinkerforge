/*
 * /*
 *  *   "TiMqWay"
 *  *
 *  *    TiMqWay(tm): A gateway to provide an MQTT-View for the Tinkerforge(tm) world (Tinkerforge-MQTT-Gateway).
 *  *
 *  *    Copyright (c) 2016 Bern University of Applied Sciences (BFH),
 *  *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *    Quellgasse 21, CH-2501 Biel, Switzerland
 *  *
 *  *    Licensed under Dual License consisting of:
 *  *    1. GNU Affero General Public License (AGPL) v3
 *  *    and
 *  *    2. Commercial license
 *  *
 *  *
 *  *    1. This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU Affero General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU Affero General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU Affero General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  *
 *  *
 *  *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *  *     accordance with the commercial license agreement provided with the
 *  *     Software or, alternatively, in accordance with the terms contained in
 *  *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *  *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *  *
 *  *
 *  *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *  *
 *  *
 */
package ch.quantasy.gateway.binding.tinkerforge.barometerV2;

import ch.quantasy.gateway.binding.tinkerforge.DeviceServiceContract;

import ch.quantasy.mdservice.message.Message;
import ch.quantasy.tinkerforge.device.TinkerforgeDeviceClass;
import ch.quantasy.tinkerforge.device.barometerV2.BarometerV2Device;
import java.util.Map;

/**
 *
 * @author reto
 */
public class BarometerV2ServiceContract extends DeviceServiceContract {

    public final String CALLBACK_CONFIG;
    public final String STATUS_SENSOR_CONFIGURATION;

    public final String AIR_PRESSURE;
    public final String STATUS_AIR_PRESSURE;
    public final String STATUS_AIR_PRESSURE_CALLBACK_CONFIG;
    public final String EVENT_AIR_PRESSURE;

    public final String ALTITUDE;
    public final String STATUS_ALTITUDE;
    public final String STATUS_ALTITUDE_CALLBACK_CONFIG;
    public final String EVENT_ALTITUDE;

    public final String STATUS_TEMPERATURE;
    public final String TEMPERATURE;
    public final String STATUS_TEMPERATURE_CALLBACK_CONFIG;
    public final String EVENT_TEMPERATURE;

    public final String MOVING_AVERAGE;
    public final String STATUS_AVERAGING;

    public final String REFERENCE_AIR_PRESSURE;
    public final String STATUS_REFERENCE_AIR_PRESSURE;
    
    public final String STATUS_CALIBRATION;

    public BarometerV2ServiceContract(BarometerV2Device device) {
        this(device.getUid(), TinkerforgeDeviceClass.getDevice(device.getDevice()).toString());
    }

    public BarometerV2ServiceContract(String id) {
        this(id, TinkerforgeDeviceClass.BarometerV2.toString());
    }

    public BarometerV2ServiceContract(String id, String device) {
        super(id, device, BarometerV2Intent.class);

        STATUS_CALIBRATION=STATUS+"/"+"calibration";
        CALLBACK_CONFIG = "callbackConfig";
        STATUS_SENSOR_CONFIGURATION=STATUS+"/"+"sensorConfiguration";

        AIR_PRESSURE = "airPressure";

        STATUS_AIR_PRESSURE = STATUS + "/" + AIR_PRESSURE;
        STATUS_AIR_PRESSURE_CALLBACK_CONFIG = STATUS_AIR_PRESSURE + "/" + CALLBACK_CONFIG;
        EVENT_AIR_PRESSURE = EVENT + "/" + AIR_PRESSURE;

        ALTITUDE = "altitude";
        STATUS_ALTITUDE = STATUS + "/" + ALTITUDE;
        STATUS_ALTITUDE_CALLBACK_CONFIG = STATUS_ALTITUDE + "/" + CALLBACK_CONFIG;
        EVENT_ALTITUDE = EVENT + "/" + ALTITUDE;

        TEMPERATURE = "temperature";
        STATUS_TEMPERATURE = STATUS + "/" + TEMPERATURE;

        STATUS_TEMPERATURE_CALLBACK_CONFIG = STATUS_TEMPERATURE + "/" + CALLBACK_CONFIG;
        EVENT_TEMPERATURE = EVENT + "/" + TEMPERATURE;

        MOVING_AVERAGE = "movingAverage";
        STATUS_AVERAGING = STATUS + "/" + MOVING_AVERAGE;

        REFERENCE_AIR_PRESSURE = "referenceAirPressure";
        STATUS_REFERENCE_AIR_PRESSURE = STATUS + "/" + REFERENCE_AIR_PRESSURE;

    }

    @Override
    public void setServiceMessageTopics(Map<String, Class<? extends Message>> messageTopicMap) {
        messageTopicMap.put(EVENT_AIR_PRESSURE, AirPressureEvent.class);
        messageTopicMap.put(EVENT_ALTITUDE, AltitudeEvent.class);
        messageTopicMap.put(EVENT_TEMPERATURE, TemperatureEvent.class);
        messageTopicMap.put(STATUS_AIR_PRESSURE_CALLBACK_CONFIG, AirPressureCallbackConfigurationStatus.class);
        messageTopicMap.put(STATUS_ALTITUDE_CALLBACK_CONFIG, AltitudeCallbackConfigurationStatus.class);
        messageTopicMap.put(STATUS_TEMPERATURE_CALLBACK_CONFIG, TemperatureCallbackConfigurationStatus.class);
        messageTopicMap.put(STATUS_AVERAGING, MovingAverageConfigurationStatus.class);
        messageTopicMap.put(STATUS_REFERENCE_AIR_PRESSURE, ReferenceAirPressureStatus.class);
        messageTopicMap.put(STATUS_CALIBRATION, CalibrationStatus.class);
        messageTopicMap.put(STATUS_SENSOR_CONFIGURATION, SensorConfigurationStatus.class);
        
    }

}
