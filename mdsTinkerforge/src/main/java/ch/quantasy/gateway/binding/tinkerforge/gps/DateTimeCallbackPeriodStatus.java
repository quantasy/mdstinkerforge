package ch.quantasy.gateway.binding.tinkerforge.gps;

import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;

public class DateTimeCallbackPeriodStatus extends AStatus {

    @Period
    public Long value;

    private DateTimeCallbackPeriodStatus() {
    }

    public DateTimeCallbackPeriodStatus(Long value) {
        this.value = value;
    }
}
