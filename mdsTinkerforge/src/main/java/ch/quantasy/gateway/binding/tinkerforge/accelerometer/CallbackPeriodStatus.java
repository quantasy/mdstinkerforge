package ch.quantasy.gateway.binding.tinkerforge.accelerometer;

import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;

public class CallbackPeriodStatus extends AStatus {

    @Period
    public Long value;

    private CallbackPeriodStatus() {
    }

    public CallbackPeriodStatus(Long value) {
        this.value = value;
    }
}
