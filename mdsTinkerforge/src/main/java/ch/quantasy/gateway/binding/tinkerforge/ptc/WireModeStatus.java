package ch.quantasy.gateway.binding.tinkerforge.ptc;

import java.lang.Short;
import ch.quantasy.mdservice.message.annotations.Choice;

import ch.quantasy.mdservice.message.AStatus;
public class WireModeStatus extends AStatus{

    @Choice(values = {"2", "3", "4"})
    public Short value;

    private WireModeStatus() {
    }

    public WireModeStatus(Short value) {
        this.value = value;
    }
}
