package ch.quantasy.gateway.binding.stackManager;

import java.lang.Boolean;
import ch.quantasy.mdservice.message.annotations.NonNull;
import ch.quantasy.mdservice.message.AStatus;

public class ConnectStatus extends AStatus {

    @NonNull()
    public Boolean value;

    private ConnectStatus() {
    }

    public ConnectStatus(Boolean value) {
        this.value = value;
    }
}
