package ch.quantasy.gateway.binding.tinkerforge.linearPoti;

import ch.quantasy.mdservice.message.annotations.Period;
import ch.quantasy.mdservice.message.AStatus;

public class PositionCallbackPeriodStatus extends AStatus {

    @Period
    public Long value;

    private PositionCallbackPeriodStatus() {
    }

    public PositionCallbackPeriodStatus(Long value) {
        this.value = value;
    }
}
