package ch.quantasy.gateway.binding.tinkerforge.loadCell;

import ch.quantasy.mdservice.message.AStatus;

public class TareStatus extends AStatus {

    public Boolean value;

    private TareStatus() {
    }

    public TareStatus(Boolean value) {
        this.value = value;
    }
}
