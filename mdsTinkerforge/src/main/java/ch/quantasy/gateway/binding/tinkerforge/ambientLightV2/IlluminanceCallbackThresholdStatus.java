package ch.quantasy.gateway.binding.tinkerforge.ambientLightV2;

import ch.quantasy.mdservice.message.AStatus;

public class IlluminanceCallbackThresholdStatus extends AStatus {

    public DeviceIlluminanceCallbackThreshold value;

    private IlluminanceCallbackThresholdStatus() {
    }

    public IlluminanceCallbackThresholdStatus(DeviceIlluminanceCallbackThreshold value) {
        this.value = value;
    }
}
