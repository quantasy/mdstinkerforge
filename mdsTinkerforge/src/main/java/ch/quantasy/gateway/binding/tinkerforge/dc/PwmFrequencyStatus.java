package ch.quantasy.gateway.binding.tinkerforge.dc;

import ch.quantasy.mdservice.message.annotations.Range;
import ch.quantasy.mdservice.message.AStatus;

public class PwmFrequencyStatus extends AStatus {

    @Range(from = 1, to = 20000)
    public Integer value;

    private PwmFrequencyStatus() {
    }

    public PwmFrequencyStatus(Integer value) {
        this.value = value;
    }
}
