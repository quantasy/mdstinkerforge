package ch.quantasy.gateway.binding.tinkerforge.remoteSwitch;

import ch.quantasy.mdservice.message.AStatus;

public class SwitchSocketCParametersStatus extends AStatus {

    public SwitchSocketCParameters value;

    private SwitchSocketCParametersStatus() {
    }

    public SwitchSocketCParametersStatus(SwitchSocketCParameters value) {
        this.value = value;
    }
}
