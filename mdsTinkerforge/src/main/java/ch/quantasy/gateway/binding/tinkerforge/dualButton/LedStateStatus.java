package ch.quantasy.gateway.binding.tinkerforge.dualButton;

import ch.quantasy.mdservice.message.AStatus;

public class LedStateStatus extends AStatus {

    public DeviceLEDState value;

    private LedStateStatus() {
    }

    public LedStateStatus(DeviceLEDState value) {
        this.value = value;
    }
}
