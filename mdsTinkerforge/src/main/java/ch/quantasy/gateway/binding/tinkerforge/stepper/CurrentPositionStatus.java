package ch.quantasy.gateway.binding.tinkerforge.stepper;
import ch.quantasy.mdservice.message.AStatus;
import ch.quantasy.mdservice.message.annotations.Range;
public class CurrentPositionStatus extends AStatus{
@Range(from = 0, to = Integer.MAX_VALUE)
public Integer value;
private CurrentPositionStatus(){}
public CurrentPositionStatus(Integer value){
  this.value=value;
}
}
