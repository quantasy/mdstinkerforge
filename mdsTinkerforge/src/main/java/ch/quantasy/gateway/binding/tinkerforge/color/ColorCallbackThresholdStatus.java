package ch.quantasy.gateway.binding.tinkerforge.color;

import ch.quantasy.mdservice.message.AStatus;

public class ColorCallbackThresholdStatus extends AStatus {

    public DeviceColorCallbackThreshold value;

    private ColorCallbackThresholdStatus() {
    }

    public ColorCallbackThresholdStatus(DeviceColorCallbackThreshold value) {
        this.value = value;
    }
}
