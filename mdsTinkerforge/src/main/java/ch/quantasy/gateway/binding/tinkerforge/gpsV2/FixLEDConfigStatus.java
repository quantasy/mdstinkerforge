package ch.quantasy.gateway.binding.tinkerforge.gpsV2;

import ch.quantasy.mdservice.message.AStatus;

public class FixLEDConfigStatus extends AStatus {

    public FixLEDConfig value;

    private FixLEDConfigStatus() {
    }

    public FixLEDConfigStatus(FixLEDConfig value) {
        this.value = value;
    }
}
