/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.quantasy.tinkerforge.device.tinkerforge.coprocessor;

import ch.quantasy.gateway.binding.tinkerforge.CoProcessor.IntentCoProcessor;
import ch.quantasy.mdservice.message.Intent;
import ch.quantasy.tinkerforge.device.tinkerforge.DeviceCallback;
import ch.quantasy.tinkerforge.device.tinkerforge.GenericDevice;
import ch.quantasy.tinkerforge.stack.TinkerforgeStack;
import com.tinkerforge.Device;
import com.tinkerforge.TinkerforgeException;
import com.tinkerforge.TimeoutException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author reto
 * @param <D>
 * @param <C>
 * @param <I>
 */
public abstract class CoProcessorDevice<D extends Device, C extends DeviceCallback, I extends Intent> extends GenericDevice<D, C, I> {

    public CoProcessorDevice(TinkerforgeStack address, D device, I intent) throws TinkerforgeException {
        super(address, device, intent);
    }

    @Override
    protected void internalAddDeviceListeners(D device) {
        super.internalAddDeviceListeners(device);
    }

    @Override
    protected void internalRemoveDeviceListeners(D device) {
        super.internalRemoveDeviceListeners(device);
    }

    @Override
    public void internalUpdate(I intent) {
        super.internalUpdate(intent);
        if (intent instanceof IntentCoProcessor) {
            IntentCoProcessor i = (IntentCoProcessor) intent;
            if (i.reset != null && i.reset == true) {
                try {
                    Method method = getDevice().getClass().getMethod("reset");
                    method.invoke(getDevice());
                } catch (Exception ex) {
                    Logger.getLogger(CoProcessorDevice.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (i.statusLEDConfig != null) {
                try {
                    Method method = getDevice().getClass().getMethod("setStatusLEDConfig",Integer.class);
                    method.invoke(getDevice(),i.statusLEDConfig.getValue());
                } catch (Exception ex) {
                    Logger.getLogger(CoProcessorDevice.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
    }

}
