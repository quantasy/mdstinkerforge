/*
 * /*
 *  *   "TiMqWay"
 *  *
 *  *    TiMqWay(tm): A gateway to provide an MQTT-View for the Tinkerforge(tm) world (Tinkerforge-MQTT-Gateway).
 *  *
 *  *    Copyright (c) 2016 Bern University of Applied Sciences (BFH),
 *  *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *    Quellgasse 21, CH-2501 Biel, Switzerland
 *  *
 *  *    Licensed under Dual License consisting of:
 *  *    1. GNU Affero General Public License (AGPL) v3
 *  *    and
 *  *    2. Commercial license
 *  *
 *  *
 *  *    1. This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU Affero General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU Affero General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU Affero General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  *
 *  *
 *  *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *  *     accordance with the commercial license agreement provided with the
 *  *     Software or, alternatively, in accordance with the terms contained in
 *  *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *  *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *  *
 *  *
 *  *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *  *
 *  *
 */
package ch.quantasy.tinkerforge.device.accelerometerV2;

import ch.quantasy.gateway.binding.tinkerforge.accelerometerV2.AccelerometerV2Intent;
import ch.quantasy.gateway.binding.tinkerforge.accelerometerV2.ContinuousAccelerationConfiguration;
import ch.quantasy.gateway.binding.tinkerforge.accelerometerV2.AccelerationCallbackConfiguration;
import ch.quantasy.gateway.binding.tinkerforge.accelerometerV2.DeviceConfiguration;

import ch.quantasy.tinkerforge.device.tinkerforge.GenericDevice;

import ch.quantasy.tinkerforge.stack.TinkerforgeStack;
import com.tinkerforge.BrickletAccelerometerV2;
import com.tinkerforge.TinkerforgeException;
import com.tinkerforge.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */
public class AccelerometerV2Device extends GenericDevice<BrickletAccelerometerV2, AccelerometerV2DeviceCallback, AccelerometerV2Intent> {

    public AccelerometerV2Device(TinkerforgeStack stack, BrickletAccelerometerV2 device) throws TinkerforgeException {
        super(stack, device, new AccelerometerV2Intent());
    }

    @Override
    protected void addDeviceListeners(BrickletAccelerometerV2 device) {
        device.addAccelerationListener(super.getCallback());
        device.addContinuousAcceleration16BitListener(super.getCallback());
        device.addContinuousAcceleration8BitListener(super.getCallback());
    }

    @Override
    protected void removeDeviceListeners(BrickletAccelerometerV2 device) {
        device.removeAccelerationListener(super.getCallback());
        device.removeContinuousAcceleration16BitListener(super.getCallback());
        device.removeContinuousAcceleration8BitListener(super.getCallback());
    }

    @Override
    public void update(AccelerometerV2Intent intent) {
        if (intent == null) {
            return;
        }
        if (!intent.isValid()) {
            return;
        }
        if (intent.continuousAccelerationConfig != null) {
            try {
                getDevice().setContinuousAccelerationConfiguration(intent.continuousAccelerationConfig.enableX,intent.continuousAccelerationConfig.enableY,intent.continuousAccelerationConfig.enableZ,intent.continuousAccelerationConfig.resolution.getValue());
                getIntent().continuousAccelerationConfig = new ContinuousAccelerationConfiguration(getDevice().getContinuousAccelerationConfiguration());
                super.getCallback().continuousAccelerationConfigChanged(getIntent().continuousAccelerationConfig);
            } catch (TinkerforgeException ex) {
                Logger.getLogger(AccelerometerV2Device.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (intent.accelerationCallbackConfiguration != null) {
            try {
                getDevice().setAccelerationCallbackConfiguration(intent.accelerationCallbackConfiguration.period,intent.accelerationCallbackConfiguration.hasToChange);
                getIntent().accelerationCallbackConfiguration = new AccelerationCallbackConfiguration(getDevice().getAccelerationCallbackConfiguration());
                super.getCallback().accelerationCallbackConfigurationChanged(getIntent().accelerationCallbackConfiguration);
            } catch (TinkerforgeException ex) {
                Logger.getLogger(AccelerometerV2Device.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (intent.deviceConfig != null) {
            try {
                getDevice().setConfiguration(intent.deviceConfig.dataRate.getValue(),intent.deviceConfig.fullScale.getValue());
                getIntent().deviceConfig = new DeviceConfiguration(getDevice().getConfiguration());
                super.getCallback().deviceConfigChanged(getIntent().deviceConfig);
            } catch (TinkerforgeException ex) {
                Logger.getLogger(AccelerometerV2Device.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
