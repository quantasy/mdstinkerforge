/*
 * /*
 *  *   "TiMqWay"
 *  *
 *  *    TiMqWay(tm): A gateway to provide an MQTT-View for the Tinkerforge(tm) world (Tinkerforge-MQTT-Gateway).
 *  *
 *  *    Copyright (c) 2016 Bern University of Applied Sciences (BFH),
 *  *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *    Quellgasse 21, CH-2501 Biel, Switzerland
 *  *
 *  *    Licensed under Dual License consisting of:
 *  *    1. GNU Affero General Public License (AGPL) v3
 *  *    and
 *  *    2. Commercial license
 *  *
 *  *
 *  *    1. This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU Affero General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU Affero General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU Affero General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  *
 *  *
 *  *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *  *     accordance with the commercial license agreement provided with the
 *  *     Software or, alternatively, in accordance with the terms contained in
 *  *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *  *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *  *
 *  *
 *  *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *  *
 *  *
 */
package ch.quantasy.tinkerforge.device.barometerV2;

import ch.quantasy.gateway.binding.tinkerforge.barometerV2.Calibration;
import ch.quantasy.gateway.binding.tinkerforge.barometerV2.DeviceAirPressureCallbackConfiguration;
import ch.quantasy.gateway.binding.tinkerforge.barometerV2.DeviceAltitudeCallbackConfiguration;
import ch.quantasy.gateway.binding.tinkerforge.barometerV2.DeviceTemperatureCallbackConfiguration;
import ch.quantasy.gateway.binding.tinkerforge.barometerV2.MovingAverageConfiguration;
import ch.quantasy.gateway.binding.tinkerforge.barometerV2.SensorConfiguration;
import ch.quantasy.tinkerforge.device.tinkerforge.DeviceCallback;
import com.tinkerforge.BrickletBarometerV2;

/**
 *
 * @author reto
 */
public interface BarometerV2DeviceCallback extends DeviceCallback, BrickletBarometerV2.AirPressureListener, BrickletBarometerV2.AltitudeListener, BrickletBarometerV2.TemperatureListener {

    public void airPressureCallbackConfigurationChanged(DeviceAirPressureCallbackConfiguration configuration);

    public void altitudeCallbackConfigurationChanged(DeviceAltitudeCallbackConfiguration configuration);
        public void temperatureCallbackConfigurationChanged(DeviceTemperatureCallbackConfiguration configuration);


    public void movingAverageConfigChanged(MovingAverageConfiguration configuration);


    public void referenceAirPressureChanged(Integer referenceAirPressure);

    public void calibrationChanged(Calibration calibration);

    public void sensorConfigurationChanged(SensorConfiguration sensorConfiguration);

}
