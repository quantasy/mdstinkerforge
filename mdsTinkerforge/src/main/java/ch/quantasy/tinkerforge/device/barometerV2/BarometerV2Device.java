/*
 * /*
 *  *   "TiMqWay"
 *  *
 *  *    TiMqWay(tm): A gateway to provide an MQTT-View for the Tinkerforge(tm) world (Tinkerforge-MQTT-Gateway).
 *  *
 *  *    Copyright (c) 2016 Bern University of Applied Sciences (BFH),
 *  *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *    Quellgasse 21, CH-2501 Biel, Switzerland
 *  *
 *  *    Licensed under Dual License consisting of:
 *  *    1. GNU Affero General Public License (AGPL) v3
 *  *    and
 *  *    2. Commercial license
 *  *
 *  *
 *  *    1. This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU Affero General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU Affero General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU Affero General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  *
 *  *
 *  *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *  *     accordance with the commercial license agreement provided with the
 *  *     Software or, alternatively, in accordance with the terms contained in
 *  *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *  *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *  *
 *  *
 *  *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *  *
 *  *
 */
package ch.quantasy.tinkerforge.device.barometerV2;

import ch.quantasy.gateway.binding.tinkerforge.barometerV2.BarometerV2Intent;
import ch.quantasy.gateway.binding.tinkerforge.barometerV2.Calibration;
import ch.quantasy.gateway.binding.tinkerforge.barometerV2.DeviceAirPressureCallbackConfiguration;
import ch.quantasy.gateway.binding.tinkerforge.barometerV2.DeviceAltitudeCallbackConfiguration;
import ch.quantasy.gateway.binding.tinkerforge.barometerV2.DeviceTemperatureCallbackConfiguration;
import ch.quantasy.gateway.binding.tinkerforge.barometerV2.MovingAverageConfiguration;
import ch.quantasy.gateway.binding.tinkerforge.barometerV2.SensorConfiguration;
import ch.quantasy.tinkerforge.device.tinkerforge.GenericDevice;
import ch.quantasy.tinkerforge.stack.TinkerforgeStack;
import com.tinkerforge.BrickletBarometerV2;
import com.tinkerforge.TinkerforgeException;
import com.tinkerforge.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */
public class BarometerV2Device extends GenericDevice<BrickletBarometerV2, BarometerV2DeviceCallback, BarometerV2Intent> {

    public BarometerV2Device(TinkerforgeStack stack, BrickletBarometerV2 device) throws TinkerforgeException {
        super(stack, device, new BarometerV2Intent());
    }

    @Override
    protected void addDeviceListeners(BrickletBarometerV2 device) {
        device.addAirPressureListener(super.getCallback());
        device.addAltitudeListener(super.getCallback());
        device.addTemperatureListener(super.getCallback());

    }

    @Override
    protected void removeDeviceListeners(BrickletBarometerV2 device) {
        device.removeAltitudeListener(super.getCallback());
        device.removeAirPressureListener(super.getCallback());
        device.removeTemperatureListener(super.getCallback());

    }

    @Override
    public void update(BarometerV2Intent intent) {
        if (intent == null) {
            return;
        }
        if (!intent.isValid()) {
            return;
        }

        if (intent.movingAverage != null) {
            try {
                getDevice().setMovingAverageConfiguration(intent.movingAverage.airPressure,intent.movingAverage.temperature);
                getIntent().movingAverage = new MovingAverageConfiguration(getDevice().getMovingAverageConfiguration());
                super.getCallback().movingAverageConfigChanged(getIntent().movingAverage);
            } catch (TinkerforgeException ex) {
                Logger.getLogger(BarometerV2Device.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (intent.airPressureCallbackConfiguration != null) {
            try {
                getDevice().setAirPressureCallbackConfiguration(intent.airPressureCallbackConfiguration.period,intent.airPressureCallbackConfiguration.valueHasToChange,intent.airPressureCallbackConfiguration.option,intent.airPressureCallbackConfiguration.min,intent.airPressureCallbackConfiguration.max);
                getIntent().airPressureCallbackConfiguration = new DeviceAirPressureCallbackConfiguration(getDevice().getAirPressureCallbackConfiguration());
                super.getCallback().airPressureCallbackConfigurationChanged(getIntent().airPressureCallbackConfiguration);
            } catch (TinkerforgeException ex) {
                Logger.getLogger(BarometerV2Device.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (intent.referenceAirPressure != null) {
            try {
                getDevice().setReferenceAirPressure(intent.referenceAirPressure);
                getIntent().referenceAirPressure = getDevice().getReferenceAirPressure();
                super.getCallback().referenceAirPressureChanged(getIntent().referenceAirPressure);
            } catch (TinkerforgeException ex) {
                Logger.getLogger(BarometerV2Device.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (intent.altitudeCallbackConfiguration != null) {
            try {
                getDevice().setAltitudeCallbackConfiguration(intent.altitudeCallbackConfiguration.period,intent.altitudeCallbackConfiguration.valueHasToChange,intent.altitudeCallbackConfiguration.option,intent.altitudeCallbackConfiguration.min,intent.altitudeCallbackConfiguration.max);
                getIntent().altitudeCallbackConfiguration = new DeviceAltitudeCallbackConfiguration(getDevice().getAltitudeCallbackConfiguration());
                super.getCallback().altitudeCallbackConfigurationChanged(getIntent().altitudeCallbackConfiguration);
            } catch (TinkerforgeException ex) {
                Logger.getLogger(BarometerV2Device.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (intent.temperatureCallbackConfiguration != null) {
            try {
                getDevice().setTemperatureCallbackConfiguration(intent.temperatureCallbackConfiguration.period,intent.temperatureCallbackConfiguration.valueHasToChange,intent.temperatureCallbackConfiguration.option,intent.temperatureCallbackConfiguration.min,intent.temperatureCallbackConfiguration.max);
                getIntent().temperatureCallbackConfiguration = new DeviceTemperatureCallbackConfiguration(getDevice().getTemperatureCallbackConfiguration());
                super.getCallback().temperatureCallbackConfigurationChanged(getIntent().temperatureCallbackConfiguration);
            } catch (TinkerforgeException ex) {
                Logger.getLogger(BarometerV2Device.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
       
        if (intent.calibration != null) {
            try {
                getDevice().setCalibration(intent.calibration.measuredAirPressure, intent.calibration.actualAirPressure);
                getIntent().calibration = new Calibration(getDevice().getCalibration());
                super.getCallback().calibrationChanged(getIntent().calibration);
            } catch (TinkerforgeException ex) {
                Logger.getLogger(BarometerV2Device.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (intent.sensorConfiguration != null) {
            try {
                getDevice().setSensorConfiguration(intent.sensorConfiguration.dataRate.getValue(),intent.sensorConfiguration.airPressureLowPassFilter.getValue());
                getIntent().sensorConfiguration = new SensorConfiguration(getDevice().getSensorConfiguration());
                super.getCallback().sensorConfigurationChanged(getIntent().sensorConfiguration);
            } catch (TinkerforgeException ex) {
                Logger.getLogger(BarometerV2Device.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
