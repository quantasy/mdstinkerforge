/*
 * /*
 *  *   "TiMqWay"
 *  *
 *  *    TiMqWay(tm): A gateway to provide an MQTT-View for the Tinkerforge(tm) world (Tinkerforge-MQTT-Gateway).
 *  *
 *  *    Copyright (c) 2016 Bern University of Applied Sciences (BFH),
 *  *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *    Quellgasse 21, CH-2501 Biel, Switzerland
 *  *
 *  *    Licensed under Dual License consisting of:
 *  *    1. GNU Affero General Public License (AGPL) v3
 *  *    and
 *  *    2. Commercial license
 *  *
 *  *
 *  *    1. This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU Affero General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU Affero General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU Affero General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  *
 *  *
 *  *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *  *     accordance with the commercial license agreement provided with the
 *  *     Software or, alternatively, in accordance with the terms contained in
 *  *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *  *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *  *
 *  *
 *  *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *  *
 *  *
 */
package ch.quantasy.tinkerforge.device.ledStripV2;

import ch.quantasy.gateway.binding.tinkerforge.ledStripV2.LEDFrame;
import ch.quantasy.gateway.binding.tinkerforge.ledStripV2.LEDStripDeviceConfig;
import ch.quantasy.gateway.binding.tinkerforge.ledStripV2.LEDValue;
import ch.quantasy.gateway.binding.tinkerforge.ledStripV2.LedStripIntent;
import ch.quantasy.tinkerforge.device.tinkerforge.GenericDevice;
import ch.quantasy.tinkerforge.stack.TinkerforgeStack;
import com.tinkerforge.BrickletLEDStripV2;
import com.tinkerforge.TinkerforgeException;
import com.tinkerforge.TimeoutException;
import java.util.Set;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */
public class LEDStripV2Device extends GenericDevice<BrickletLEDStripV2, LEDStripV2DeviceCallback, LedStripIntent> implements BrickletLEDStripV2.FrameStartedListener {

    public static final int DEFAULT_NUMBER_OF_LEDS = 500;

    // Frame duration in Milliseconds 20ms (1000ms / 50frames = 20ms per frame)
    public static final int DEFAULT_FRAME_DURATION_IN_MILLISECONDS = 1000;

    // IC-refresh in Hz
    public static final int DEFAULT_CLOCK_FREQUENCY_OF_ICS_IN_HZ = 2000000;

    // ChipType
    public static final LEDStripDeviceConfig.ChipType DEFAULT_CHIP_TYPE = LEDStripDeviceConfig.ChipType.WS2801;

    public static final LEDStripDeviceConfig.ChannelMapping DEFAULT_CHANNEL_MAPPING = LEDStripDeviceConfig.ChannelMapping.RGB;

    private final BlockingDeque<LEDFrame> publishingQueue;
    private final FrameRenderer renderer;
    private Thread rendererThread;

    private final Object deviceLock = new Object();

    public LEDStripV2Device(TinkerforgeStack stack, BrickletLEDStripV2 device) throws TinkerforgeException {
        super(stack, device, new LedStripIntent());
        this.publishingQueue = new LinkedBlockingDeque<>();
        renderer = new FrameRenderer(this.publishingQueue);
        getIntent().config = new LEDStripDeviceConfig(LEDStripV2Device.DEFAULT_CHIP_TYPE, LEDStripV2Device.DEFAULT_CLOCK_FREQUENCY_OF_ICS_IN_HZ, LEDStripV2Device.DEFAULT_FRAME_DURATION_IN_MILLISECONDS, LEDStripV2Device.DEFAULT_NUMBER_OF_LEDS, LEDStripV2Device.DEFAULT_CHANNEL_MAPPING);
    }

    @Override
    protected void addDeviceListeners(BrickletLEDStripV2 device) {
        device.addFrameStartedListener(this);
        try {
            this.setup();
        } catch (TinkerforgeException ex) {
            Logger.getLogger(LEDStripV2Device.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    protected void removeDeviceListeners(BrickletLEDStripV2 device) {
        device.removeFrameStartedListener(this);
    }

    private void setup() throws TinkerforgeException {
        setChipType(getIntent().config.chipType);
        setClockFrequencyOfICsInHz(getIntent().config.clockFrequencyOfICsInHz);
        setFrameDurationInMilliseconds(getIntent().config.frameDurationInMilliseconds);
        setChannelMapping(getIntent().config.channelMapping);
        setNumberOfLEDs(getIntent().config.numberOfLEDs);
        if (rendererThread == null) {
            rendererThread = new Thread(renderer);
            //publisherThread.setDaemon(true);
            rendererThread.start();
        }

    }

    @Override
    public void update(LedStripIntent intent) {
        if (intent == null) {
            return;
        }
        if (!intent.isValid()) {
            return;
        }

        if (intent.config != null && !(getIntent().config.equals(intent.config)) && intent.config.isValid()) {
            try {
                LEDStripDeviceConfig config = intent.config;
                if (getIntent().config.chipType != config.chipType) {
                    setChipType(config.chipType);
                }
                if (getIntent().config.clockFrequencyOfICsInHz != config.clockFrequencyOfICsInHz) {
                    setClockFrequencyOfICsInHz(config.clockFrequencyOfICsInHz);
                }
                if (getIntent().config.frameDurationInMilliseconds != config.frameDurationInMilliseconds) {
                    setFrameDurationInMilliseconds(config.frameDurationInMilliseconds);
                }
                if (getIntent().config.channelMapping != config.channelMapping) {
                    setChannelMapping(config.channelMapping);
                }
                if (getIntent().config.numberOfLEDs != config.numberOfLEDs) {
                    setNumberOfLEDs(config.numberOfLEDs);
                }
                super.getCallback().configurationChanged(getIntent().config);
            } catch (TinkerforgeException ex) {
                Logger.getLogger(LEDStripV2Device.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (intent.LEDFrame != null) {
            add(intent.LEDFrame);
        }
        if (intent.LEDFrames != null) {
            add(intent.LEDFrames);
        }
        if (intent.LEDs != null) {
            add(intent.LEDs, getIntent().config);
        }

    }

    private void setFrameDurationInMilliseconds(final int frameDurationInMilliseconds) throws TinkerforgeException {
        synchronized (deviceLock) {
            if(getIntent().config.frameDurationInMilliseconds==frameDurationInMilliseconds){
                return;
            }
            if (getDevice() != null) {
                getDevice()
                        .setFrameDuration(frameDurationInMilliseconds);
                //Querying the real device is too slow, hence we will write it blindly to the config
                //getIntent().config.frameDurationInMilliseconds = getDevice().getFrameDuration();
                getIntent().config.frameDurationInMilliseconds = frameDurationInMilliseconds;
            }

        }
    }

    private void setNumberOfLEDs(int numberOfLEDs) {
        synchronized (deviceLock) {
            getIntent().config.numberOfLEDs
                    = numberOfLEDs;

        }
    }

    private void setChipType(LEDStripDeviceConfig.ChipType chipType) throws TinkerforgeException {
        synchronized (deviceLock) {
            if (getDevice() != null) {
                getDevice().setChipType(chipType.type
                );
                getIntent().config.chipType = chipType;
            }
        }
    }

    private void setClockFrequencyOfICsInHz(final long clockFrequencyOfICsInHz) throws TinkerforgeException {
        synchronized (deviceLock) {
            if (getDevice() != null) {
                getDevice().setClockFrequency(clockFrequencyOfICsInHz);
                getIntent().config.clockFrequencyOfICsInHz
                        = getDevice().getClockFrequency();
            }

        }
    }

    private void setChannelMapping(LEDStripDeviceConfig.ChannelMapping channelMapping) throws TinkerforgeException {
        synchronized (deviceLock) {
            if (getDevice() != null) {
                getDevice().setChannelMapping(channelMapping.mapping);
                getIntent().config.channelMapping = LEDStripDeviceConfig.ChannelMapping
                        .getChannelMappingFor(getDevice().getChannelMapping());

            }

        }
    }

    @Override
    public void frameStarted(final int length) {
        if (renderer.isRendered()) {
            super.getCallback().frameRendered(publishingQueue.size());
        }
        renderer.renderNextFrame();
        renderer.cleared();
    }

    public void add(Set<LEDValue> ledValues, LEDStripDeviceConfig config) {
        LEDFrame frame = new LEDFrame(ledValues, config);
        if (ledFrame == null) {
            ledFrame = frame;
        } else {
            ledFrame.setColor(ledValues);
        }
        add(ledFrame);

    }
    private LEDFrame ledFrame = null;

    public void add(LEDFrame frame) {
        renderer.clear();
        publishingQueue.offer(frame);
        try {
            synchronized (deviceLock) {
                ////get rid of the displayed frame, hence speedup frameDuration
                ////the frameDuration will be re-set when rendering the next frame.
                ////int frameDuration=getDevice().getFrameDuration();
                renderer.renderNextFrame();
                ////getDevice().setFrameDuration(frameDuration);
            }
        } catch (Exception ex) {
            Logger.getLogger(LEDStripV2Device.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void add(LEDFrame[] frames) {
        for (LEDFrame frame : frames) {
            publishingQueue.offer(frame);
        }
    }

    class FrameRenderer implements Runnable {

        private boolean rendered = false;
        private boolean renderNextFrame = true;
        private final BlockingDeque<LEDFrame> publishingQueue;

        public FrameRenderer(BlockingDeque<LEDFrame> publishingQueue) {
            this.publishingQueue = publishingQueue;
        }

        public boolean isRendered() {
            return rendered;
        }
        private boolean clear = false;

        public synchronized void cleared() {
            clear = false;
        }

        public synchronized void clear() {
            publishingQueue.clear();
            clear = true;
        }

        public synchronized void renderNextFrame() {
            renderNextFrame = true;
            rendered = false;
            notifyAll();
        }

        @Override
        public void run() {
            while (true) {
                try {
                    synchronized (this) {
                        while (!renderNextFrame) {
                            wait(100);
                        }
                        renderNextFrame = false;
                    }
                    final LEDFrame frame = publishingQueue.take();

                    if (frame == null) {
                        frameStarted(0);
                        return;
                    }
                    LEDStripDeviceConfig localConfig = getIntent().config;
                    if (localConfig == null) {
                        frameStarted(0);
                        return;
                    }
                    if (localConfig.numberOfLEDs != frame.getNumberOfLEDs()) {
                        frameStarted(0);
                        return;
                    }
                    if (localConfig.chipType.numberOfChannels != frame.getNumberOfChannels()) {
                        frameStarted(0);
                        return;
                    }
                    if (clear) {
                        synchronized (deviceLock) {
                            getDevice().setFrameDuration(20);
                            getDevice().setLEDValues(0, frame.getColors());

                        }
                        renderNextFrame = true;
                    } else {
                        if (frame.durationInMillis != null) {
                            setFrameDurationInMilliseconds(frame.durationInMillis);
                        } else {
                            setFrameDurationInMilliseconds(localConfig.frameDurationInMilliseconds);
                        }
                        synchronized (deviceLock) {
                            getDevice().setLEDValues(0, frame.getColors());
                        }
                    }
                    rendered = true;

                } catch (Exception ex) {
                    Logger.getLogger(LEDStripV2Device.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

}
