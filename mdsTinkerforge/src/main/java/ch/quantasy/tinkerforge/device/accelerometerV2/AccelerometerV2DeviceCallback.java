/*
 * /*
 *  *   "TiMqWay"
 *  *
 *  *    TiMqWay(tm): A gateway to provide an MQTT-View for the Tinkerforge(tm) world (Tinkerforge-MQTT-Gateway).
 *  *
 *  *    Copyright (c) 2016 Bern University of Applied Sciences (BFH),
 *  *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *    Quellgasse 21, CH-2501 Biel, Switzerland
 *  *
 *  *    Licensed under Dual License consisting of:
 *  *    1. GNU Affero General Public License (AGPL) v3
 *  *    and
 *  *    2. Commercial license
 *  *
 *  *
 *  *    1. This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU Affero General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU Affero General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU Affero General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  *
 *  *
 *  *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *  *     accordance with the commercial license agreement provided with the
 *  *     Software or, alternatively, in accordance with the terms contained in
 *  *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *  *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *  *
 *  *
 *  *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *  *
 *  *
 */
package ch.quantasy.tinkerforge.device.accelerometerV2;

import ch.quantasy.gateway.binding.tinkerforge.accelerometerV2.AccelerationCallbackConfiguration;
import ch.quantasy.gateway.binding.tinkerforge.accelerometerV2.ContinuousAccelerationConfiguration;
import ch.quantasy.tinkerforge.device.tinkerforge.DeviceCallback;
import com.tinkerforge.BrickletAccelerometerV2;

/**
 *
 * @author reto
 */
public interface AccelerometerV2DeviceCallback extends DeviceCallback, BrickletAccelerometerV2.AccelerationListener, BrickletAccelerometerV2.ContinuousAcceleration16BitListener,BrickletAccelerometerV2.ContinuousAcceleration8BitListener {

    public void continuousAccelerationConfigChanged(ContinuousAccelerationConfiguration continuousAccelerationConfig);

    public void accelerationCallbackConfigurationChanged(AccelerationCallbackConfiguration accelerationCallbackConfiguration);

    public void deviceConfigChanged(ch.quantasy.gateway.binding.tinkerforge.accelerometerV2.DeviceConfiguration deviceConfig);
}
